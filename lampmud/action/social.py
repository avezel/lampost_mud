from lampost.di.app import on_app_start
from lampost.di.resource import Injected, module_inject
from lampost.gameops.action import make_action

from lampmud.action.store import mud_action

log = Injected('log')
db = Injected('datastore')
mud_actions = Injected('mud_actions')
module_inject(__name__)

all_socials = {}


@on_app_start
def _start():
    for social in db.load_object_set('social'):
        add_social_action(social)


def add_social_action(social):
    if mud_actions.primary(social.dbo_id):
        log.warn("Mud action already exists for social id {}", social.dbo_id)
    else:
        mud_actions.add(make_action(social, social.dbo_id))
        all_socials[social.dbo_id] = social


def remove_social_action(social):
    all_socials.pop(social.dbo_id)
    mud_actions.remove(social)


@mud_action('socials')
def socials_action(**_):
    socials = sorted(all_socials.keys())
    if socials:
        return " ".join(socials)
    return "No socials created!"
