from lampost.di.resource import Injected, module_inject
from lampost.util.lputil import ClientError
from lampost.gameops.action import ActionError

from lampmud.action.store import mud_action

log = Injected('log')
db = Injected('datastore')
ev = Injected('dispatcher')
perm = Injected('perm')
um = Injected('user_manager')
message_service = Injected('message_service')
friend_service = Injected('friend_service')
mud_actions = Injected('mud_actions')
module_inject(__name__)


@mud_action('help', target_class='cmd_str_opt')
def help_action(source, target):
    if not target:
        source.display_line('Available actions:')
        verb_lists = ["/".join(verbs) for verbs in mud_actions.all_actions().values()]
        return source.display_line(", ".join(sorted(verb_lists)))
    actions = mud_actions.primary(target)
    if not actions:
        actions = mud_actions.abbrev(target)
    if not actions:
        raise ActionError("No matching command found")
    if len(actions) > 1:
        raise ActionError("Multiple matching actions")
    return getattr(actions[0], "help_text", "No help available.")


@mud_action(('quit', 'log out'))
def quit_action(source):
    source.check_logout()
    ev.dispatch('session_logout', source.session)


@mud_action(("l", "look", "examine", "look at"), "examine")
def look(source, target):
    return target.examine(source)


@mud_action('friends')
def friends(source):
    friend_list = friend_service.friend_list(source.dbo_id)
    return "Your friends are:<br/>&nbsp&nbsp{}".format(friend_list) if friend_list else "Alas, you are friendless."


@mud_action('blocks')
def blocks(source):
    blocked = message_service.block_list(source.dbo_id)
    return "You have blocked:<br/>&nbsp&nbsp{}".format(blocked) if blocked else "You have not blocked anyone."


@mud_action('friend', target_class='player_online')
def friend(source, target):
    if message_service.is_blocked(target.dbo_id, source.dbo_id):
        raise ActionError("You are blocked from sending messages to {}.".format(target.name), 'system')
    if friend_service.is_friend(source.dbo_id, target.dbo_id):
        return "{} is already your friend.".format(target.name)
    try:
        friend_service.friend_request(source, target)
    except ClientError as exp:
        return str(exp)
    return "Friend request sent"


@mud_action('unfriend', target_class='player_env player_db')
def unfriend(source, target):
    if friend_service.is_friend(source.dbo_id, target.dbo_id):
        friend_service.del_friend(source.dbo_id, target.dbo_id)
        message_service.add_message('system', "You unfriended {}.".format(target.name), source.dbo_id)
        message_service.add_message('system', "{} unfriended you!".format(source.name), target.dbo_id)
    else:
        raise ActionError("{} is not your friend".format(target.name))


@mud_action('message', target_class='player_env player_db self', obj_class='cmd_str')
def message(source, target, obj):
    message_service.add_message('player', obj, target.dbo_id, source.dbo_id)
    return "Message Sent"


@mud_action('block', target_class='player_env player_db')
def block(source, target):
    if message_service.is_blocked(source.dbo_id, target.dbo_id):
        return "You have already blocked {}.".format(target.name)
    message_service.block_messages(source.dbo_id, target.dbo_id)
    return "You have blocked messages from {}.".format(target.name)


@mud_action('unblock', target_class='player_env player_db')
def unblock(source, target):
    if message_service.is_blocked(source.dbo_id, target.dbo_id):
        message_service.unblock_messages(source.dbo_id, target.dbo_id)
        return "You unblock messages from {}".format(target.name)
    return "You are not blocking messages from {}".format(target.name)


@mud_action('follow', 'followers')
def follow(source, target):
    if hasattr(source, 'following'):
        return "You are already following {}.".format(source.following.name)
    source.follow(target)
    source.following = target


@mud_action('unfollow', target_class="cmd_str_opt")
def unfollow(source, target):
    if not hasattr(source, 'following'):
        return "You aren't following anyone."
    if target and target.lower() != source.following.name.lower():
        return "You aren't following {}.".format(target)
    source.unfollow()


@mud_action('abandon')
def abandon(source):
    if source.dead:
        source.resurrect()
    else:
        source.display_line("You're not dead yet!")


@mud_action(('get', 'pick up'), 'get', target_class="env_items", quantity=True)
def get(source, target, quantity=None):
    target.get(source, quantity)


@mud_action(('drop', 'put down'), 'drop', target_class="inven", quantity=True)
def drop(source, target, quantity=None):
    target.drop(source, quantity)


@mud_action(('i', 'inventory'))
def inven(source):
    if source.inven:
        source.display_line("You are carrying:")
        for article in source.inven:
            source.display_line("&nbsp;&nbsp;{}".format(article.short_desc(source)))
    else:
        source.display_line("You aren't carrying anything.")

