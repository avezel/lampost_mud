from lampost.di.resource import register
from lampost.gameops.action import ActionStore, make_action

_mud_actions = ActionStore()
imm_actions = set()

register('mud_actions', _mud_actions)


def mud_action(verbs, msg_class=None, **kwargs):
    def dec_wrapper(func):
        action = make_action(func, verbs, msg_class, **kwargs)
        _mud_actions.add_unique(action)
    return dec_wrapper


def imm_action(verbs, msg_class=None, imm_level='builder', **kwargs):
    def dec_wrapper(func):
        imm_actions.add(func)
        func.imm_level = imm_level
        return make_action(func, verbs, msg_class, **kwargs)
    return dec_wrapper
