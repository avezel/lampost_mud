from random import randint

from lampost.db.dbo import PropertyDBO, OwnerDBO
from lampost.db.dbofield import DBOField, DBOTField
from lampost.db.template import Template, TemplateInstance
from lampost.di.resource import Injected, module_inject
from lampost.gameops.action import ActionError
from lampost.meta.auto import TemplateField

log = Injected('log')
db = Injected('datastore')
ev = Injected('dispatcher')
module_inject(__name__)


def roll_calc(source, calc, skill_level=0):
    base_calc = sum(getattr(source, attr, 0) * calc_value for attr, calc_value in calc.items())
    roll = randint(0, 20)
    if roll == 0:
        roll = -5
    if roll == 19:
        roll = 40
    return base_calc + roll * calc.get('roll', 0) + skill_level * calc.get('skill', 0)


def avg_calc(source, calc, skill_level=0):
    base_calc = sum(getattr(source, attr, 0) * calc_value for attr, calc_value in calc.items())
    return base_calc + 10 * calc.get('roll', 0) + skill_level * calc.get('skill', 0)


class SkillTemplate(OwnerDBO, Template):

    def _on_hydrated(self):
        if not self.auto_start:
            self.verbs = self.verb


class DefaultSkill(PropertyDBO):
    class_id = 'default_skill'
    skill_template = DBOField(dbo_class_id='untyped', required=True)
    skill_level = DBOField(1)


class BaseSkill(TemplateInstance):
    verb = DBOTField()
    name = DBOTField()
    desc = DBOTField()
    prep_time = DBOTField(0)
    cool_down = DBOTField(0)
    pre_reqs = DBOTField([])
    costs = DBOTField({})
    prep_map = DBOTField({})
    display = DBOTField('default')
    auto_start = DBOTField(False)
    skill_level = DBOField(1)
    skill_source = DBOField()
    last_used = DBOField(0)
    verbs = TemplateField()

    def prepare_action(self, source, target):
        if self.available > 0:
            raise ActionError("You cannot {} yet.".format(self.verb))
        self.validate(source, target)
        if self.prep_map and self.prep_time:
            source.broadcast(verb=self.verb, display=self.display, target=target, **self.prep_map)

    def validate(self, source, target, **kwargs):
        pass

    def use(self, source, **kwargs):
        source.apply_costs(self.costs)
        self.invoke(source, **kwargs)
        self.last_used = ev.current_pulse

    def invoke(self, source, **kwargs):
        pass

    def revoke(self, source):
        pass

    @property
    def available(self):
        return self.last_used + self.cool_down - ev.current_pulse

    def __call__(self, source, **kwargs):
        self.use(source, **kwargs)


