from lampost.db.dbofield import DBOTField
from lampost.gameops import target
from lampost.gameops.action import ActionError
from lampost.meta.auto import TemplateField

from lampmud.engine.tools import combat_log
from lampmud.lpmud.engine.combat import validate_weapon, Attack, damage_categories, validate_delivery, validate_dam_type
from lampmud.lpmud.model.skill import SkillTemplate, BaseSkill, avg_calc, roll_calc


class AttackTemplate(SkillTemplate):
    dbo_key_type = 'attack'
    dbo_set_key = 'attacks'


class AttackSkill(BaseSkill):
    template_id = 'attack'

    target_class = target.make_gen('env_living')

    display = 'combat'
    msg_class = 'attacked'
    match_args = 'source', 'target'
    damage_type = DBOTField('weapon')
    delivery = DBOTField('melee')
    damage_calc = DBOTField({})
    damage_pool = DBOTField('health')
    accuracy_calc = DBOTField({})
    weapon_type = DBOTField('any')
    prep_map = DBOTField(
        {'s': 'You prepare to {v} {N}.', 't': '{n} prepares to {v} you.', 'e': '{n} prepares to {v} {N}.'})
    success_map = DBOTField(
        {'s': 'You {v} {N}.', 't': '{n} {v}s you.', 'e': '{n} {v}s {N}.', 'display': 'combat'})
    fail_map = DBOTField(
        {'s': 'You miss {N}.', 't': '{n} misses you.', 'e': '{n} missed {N}.', 'display': 'combat'})

    def validate(self, source, target, **kwargs):
        if source == target:
            raise ActionError("You cannot harm yourself.  This is a happy place.")
        if validate_weapon(self, source.weapon_type):
            self.active_damage_type = source.weapon.damage_type
        else:
            self.active_damage_type = self.damage_type
        if 'dual_wield' in self.pre_reqs:
            validate_weapon(self, source.second_type)

    def prepare_action(self, source, target):
        self.validate(source, target)
        source.start_combat(target)
        target.start_combat(source)
        super().prepare_action(source, target)

    def invoke(self, source, target):
        attack = Attack().from_skill(self, source)
        combat_log(source, attack)
        target.attacked(source, attack)

    def points_per_pulse(self, owner):
        effect = avg_calc(owner, self.accuracy_calc, self.skill_level) + avg_calc(owner, self.damage_calc,
                                                                                  self.skill_level)
        cost = avg_calc(owner, self.costs, self.skill_level)
        return int((effect - cost) / max(self.prep_time, 1))


class DefenseTemplate(SkillTemplate):
    dbo_key_type = 'defense'
    dbo_set_key = 'defenses'

    def _on_hydrated(self):
        self.calc_damage_types = set()
        for damage_type in self.damage_type:
            try:
                self.calc_damage_types |= damage_categories[damage_type]
            except KeyError:
                self.calc_damage_types.add(damage_type)


class DefenseSkill(BaseSkill):
    template_id = 'defense'

    target_class = target.make_gen('env_living')
    damage_type = DBOTField(['physical'])
    delivery = DBOTField(['melee', 'ranged'])
    weapon_type = DBOTField('unused')
    absorb_calc = DBOTField({})
    avoid_calc = DBOTField({})
    success_map = DBOTField(
        {'s': 'You avoid {N}\'s attack.', 't': '{n} avoids your attack.', 'e': '{n} avoids {N}\'s attack.'})
    calc_damage_types = TemplateField([])

    match_args = 'source',

    def invoke(self, source):
        source.defenses.add(self)

    def revoke(self, source):
        if self in source.defenses:
            source.defenses.remove(self)

    def apply(self, owner, attack):
        try:
            validate_weapon(self, owner)
            validate_delivery(self, attack.delivery)
            validate_dam_type(self, attack.damage_type)
        except ActionError:
            return
        adj_accuracy = roll_calc(owner, self.avoid_calc, self.skill_level)
        combat_log(attack.source, lambda: ''.join(['{N} defense: ', self.name, ' adj_accuracy: ', str(adj_accuracy)]),
                   self)
        attack.adj_accuracy -= adj_accuracy
        if attack.adj_accuracy < 0:
            return
        absorb = roll_calc(owner, self.absorb_calc, self.skill_level)
        combat_log(attack.source, lambda: ''.join(['{N} defense: ', self.name, ' absorb: ', str(absorb)]), self)
        attack.adj_damage -= absorb

    def points_per_pulse(self, owner):
        effect = avg_calc(owner, self.avoid_calc, self.skill_level) + avg_calc(owner, self.absorb_calc,
                                                                               self.skill_level)
        cost = avg_calc(owner, self.costs, self.skill_level)
        return int((effect - cost) / max(self.prep_time, 1))
