from lampost.server.link import add_link_object

from lampmud.lpmud.editor.skill import SkillEditor


def add_editor_routes():
    add_link_object('editor/attack', SkillEditor('attack'))
    add_link_object('editor/defense', SkillEditor('defense'))
