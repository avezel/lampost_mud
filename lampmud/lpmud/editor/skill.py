from lampost.editor.editor import Editor


def _ensure_name(obj_def):
        name = obj_def['name'] or obj_def['verb'] or obj_def['dbo_id']
        obj_def['name'] = name.capitalize()


class SkillEditor(Editor):
    def _pre_create(self, obj_def, *_):
        _ensure_name(obj_def)

    def _pre_update(self, obj_def, *_):
        _ensure_name(obj_def)
