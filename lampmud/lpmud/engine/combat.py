from lampost.di.app import on_app_start
from lampost.di.config import config_value, on_config_change
from lampost.di.resource import Injected, module_inject
from lampost.gameops.action import ActionError
from lampost.meta.auto import TemplateField
from lampost.meta.core import CoreMeta
from lampost.util.lputil import args_print

from lampmud.lpmud.model import attributes
from lampmud.lpmud.model.skill import roll_calc

log = Injected('log')
ev = Injected('dispatcher')
module_inject(__name__)

damage_categories = {}


@on_app_start
@on_config_change
def _config():
    global damage_categories
    damage_categories = {group['dbo_id']: set() for group in config_value('damage_groups')}
    for damage_type in config_value('damage_types'):
        dbo_id, damage_group = damage_type['dbo_id'], damage_type['group']
        if damage_group:
            damage_categories['any'].add(dbo_id)
            damage_categories[damage_group].add(dbo_id)


CON_LEVELS = ['Insignificant', 'Trivial', 'Pesky', 'Annoying', 'Irritating', 'Bothersome', 'Troublesome',
              'Evenly Matched',
              'Threatening', 'Difficult', 'Intimidating', 'Imposing', 'Frightening', 'Terrifying', 'Unassailable']
CON_RANGE = int((len(CON_LEVELS) - 1) / 2)


def validate_weapon(ability, weapon_type):
    if not ability.weapon_type or ability.weapon_type == 'unused':
        return
    if ability.weapon_type == 'unarmed':
        if weapon_type:
            raise ActionError("You can't do that with a weapon.")
        return
    if not weapon_type:
        raise ActionError("That requires a weapon.")
    if ability.weapon_type != 'any' and ability.weapon_type != weapon_type:
        raise ActionError("You need a different weapon for that.")
    return True


def validate_delivery(ability, delivery):
    if delivery not in ability.delivery:
        raise ActionError("This doesn't work against that.")


def validate_dam_type(ability, damage_type):
    if damage_type not in ability.calc_damage_types:
        raise ActionError("That has no effect.")


def calc_consider(entity):
    try:
        best_attack = max(
            [skill.points_per_pulse(entity) for skill in entity.skills.values() if skill.template_id == 'attack'])
    except ValueError:
        best_attack = 0
    try:
        best_defense = max(
            [skill.points_per_pulse(entity) for skill in entity.skills.values() if skill.template_id == 'defense'])
    except ValueError:
        best_defense = 0
    pool_levels = sum(getattr(entity, base_pool_id, 0) for pool_id, base_pool_id in attributes.pool_keys)
    return int((best_attack + best_defense + pool_levels) / 2)


def consider_level(source_con, target_con):
    perc = max(target_con, 1) / max(source_con, 1) * 100
    perc = min(perc, 199)
    return int(perc / 13.27) - CON_RANGE


class Attack(metaclass=CoreMeta):
    success_map = TemplateField()
    fail_map = TemplateField()
    delivery = TemplateField()
    damage_pool = TemplateField()
    verb = TemplateField()

    def from_skill(self, skill, source):
        self.template = skill
        self.damage_type = skill.active_damage_type
        self.accuracy = roll_calc(source, skill.accuracy_calc, skill.skill_level)
        self.damage = roll_calc(source, skill.damage_calc, skill.skill_level)
        self.adj_damage = self.damage
        self.adj_accuracy = self.accuracy
        self.source = source
        return self

    def combat_log(self):
        return ''.join(['{n} ATTACK-- ',
                        args_print(damage_type=self.damage_type, accuracy=self.accuracy,
                                   damage=self.damage)])
