from lampmud.action.store import mud_action


@mud_action(('wear', 'equip', 'wield'), 'equip_slot', target_class="inven")
def wear(source, target, **_):
    source.equip_article(target)


@mud_action(('remove', 'unequip', 'unwield'), 'current_slot')
def remove(source, target,  **_):
    source.remove_article(target)
