from lampmud.action.store import mud_action
from lampmud.lpmud.engine.combat import CON_LEVELS, consider_level, CON_RANGE


@mud_action('consider', 'considered', target_class='env_living')
def consider(source, target, **_):
    target_con = target.considered()
    source_con = source.considered()
    con_string = CON_LEVELS[consider_level(source_con, target_con) + CON_RANGE]
    saved_last = source.last_opponent
    source.last_opponent = target
    source.status_change()
    source.last_opponent = saved_last
    return "At first glance, {} looks {}.".format(target.name, con_string)


@mud_action('peace')
def peace(source):
    if source.fight.opponents:
        source.fight.end_all()
        return "You use your great calming power to end the fight."
    else:
        return "You're not in combat"
