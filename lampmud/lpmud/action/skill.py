from lampost.db.registry import dbo_types
from lampost.gameops.action import ActionError

from lampmud.action.store import mud_action, imm_action
from lampmud.lpmud.model.skill import SkillTemplate, db, log


@mud_action("skills", "skills", target_class="living")
def skills(source, target):
    source.display_line("{}'s Skills:".format(target.name))

    for skill_id, skill in target.skills.items():
        source.display_line("{}:   Level: {}".format(skill.name, str(skill.skill_level)))
        source.display_line("--{}".format(skill.desc if skill.desc else 'No Description'))


@imm_action("add skill", target_class="cmd_str", prep="to", obj_msg_class="skills", obj_class="living", self_object=True)
def add_skill_action(target, obj):
    skill_args = target.split(' ')
    try:
        skill_name = skill_args[0]
    except IndexError:
        raise ActionError("Skill name required")
    try:
        skill_level = int(skill_args[1])
    except (IndexError, ValueError):
        skill_level = 1
    if ':' in skill_name:
        skill_id = skill_name
    else:
        skill_id = None
        for skill_type in dbo_types(SkillTemplate):
            if skill_name in db.fetch_set_keys(skill_type.dbo_set_key):
                skill_id = '{}:{}'.format(skill_type.dbo_key_type, skill_name)
                break
    if skill_id:
        skill_template = db.load_object(skill_id)
        if skill_template:
            add_skill(skill_template, obj, skill_level, 'immortal')
            if getattr(obj, 'dbo_id', None):
                db.save_object(obj)
            return "Added {} to {}".format(skill_name, obj.name)
    return "Skill {} not found ".format(skill_name)


@imm_action("remove skill", target_class="cmd_str", prep="from", obj_msg_class="skills", obj_class="living", self_object=True)
def remove_skill(target, obj):
    if ':' in target:
        skill_id = target
    else:
        skill_id = None
        for skill_type in dbo_types(SkillTemplate):
            skill_id = '{}:{}'.format(skill_type.dbo_key_type, target)
            if skill_id in obj.skills:
                break
            else:
                skill_id = None
    obj.remove_skill(skill_id)
    if getattr(obj, 'dbo_id', None):
        db.save_object(obj)
    return "Removed {} from {}".format(target, obj.name)


def add_skill(skill_template, target, skill_level, skill_source=None):
    if skill_template:
        skill_instance = skill_template.create_instance(target)
        skill_instance.skill_level = skill_level
        skill_instance.skill_source = skill_source
        target.add_skill(skill_instance)
        return skill_instance
    log.warn('Unable to add missing skill {}', skill_template.dbo_id)
