def flavor_setup(args):

    from lampost.di import resource
    from lampost.di.config import config_value

    from lampmud.lpmud.model import init_model
    from lampmud.lpmud.model.archetype import PlayerRace
    from lampmud.lpmud.model import attributes

    db = resource.get_resource('datastore')

    init_model()
    root_area = config_value('root_area_id')
    room_id = "{0}:0".format(root_area)
    imm_name = args.imm_name.lower()

    db.create_object('area', {'dbo_id': root_area, 'name': root_area, 'next_room_id': 1})
    db.create_object('room', {'dbo_id': room_id, 'title': "Immortal Start Room", 'desc': "A brand new start room for immortals."})

    attributes.init()

    race_dto = PlayerRace.new_dto()
    race_dto.update(config_value('default_player_race'))
    race = db.create_object(PlayerRace, race_dto)

    supreme_level = config_value('imm_levels')['supreme']
    player_data = {'dbo_id': imm_name, 'room_id': room_id, 'race': race.dbo_id, 'home_room': room_id, 'imm_level': supreme_level}
    return player_data


def flavor_start(args):
    from lampost.di import resource

    from lampmud.lpmud.action import system
    from lampmud.lpmud.editor.server import add_editor_routes
    from lampmud.lpmud.model import init_model
    from lampmud.lpmud.server import add_routes

    resource.register('action_system', system)
    init_model()
    add_routes()
    add_editor_routes()

