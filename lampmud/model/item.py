from lampost.event.attach import Attachable
from lampost.gameops.target import TargetKeys
from lampost.meta.auto import TemplateField
from lampost.db.dbo import PropertyDBO, DBOAspect
from lampost.db.dbofield import DBOField, DBOTField
from lampost.db.template import TemplateInstance
from lampost.gameops.action import obj_action, ActionProvider


def target_keys(item):
    t_keys = TargetKeys(item.title)
    for alias in item.aliases:
        t_keys.add(alias)
    return t_keys


class ItemAspect(DBOAspect, ActionProvider, Attachable):
    sex = DBOField('none')
    flags = DBOField({})
    title = ''
    aliases = []

    living = False
    env = None

    def _on_hydrated(self):
        if not self.target_keys:
            self.target_keys = target_keys(self)

    @property
    def name(self):
        return self.title

    def short_desc(self, observer=None):
        return self.title

    def long_desc(self, observer=None):
        return self.desc if self.desc else self.title

    def examine(self, source):
        if source.can_see(self):
            source.display_line(self.long_desc(source))

    def receive_broadcast(self, broadcast):
        pass

    def social(self, social):
        pass

    def leave_env(self):
        pass


class ItemDBO(PropertyDBO, ItemAspect, Attachable):
    class_id = 'base_item'

    desc = DBOField('')
    title = DBOField('')
    aliases = DBOField([])

    target_keys = None


class InvenContainer(ItemDBO):
    class_id = 'container'

    contents = DBOField([], 'untyped')

    def __iter__(self):
        for item in self.contents:
            yield item

    def __len__(self):
        return len(self.contents)

    def __contains__(self, item):
        return item in self.contents

    def __getitem__(self, key):
        return self.contents[key]

    def append(self, item):
        self.contents.append(item)

    def remove(self, item):
        self.contents.remove(item)


class ItemInstance(TemplateInstance, ItemAspect):
    desc = DBOTField('')
    title = DBOTField('')
    aliases = DBOTField([])

    target_keys = TemplateField()


class Readable(DBOAspect, ActionProvider):
    class_id = 'readable'

    text = DBOField('')

    @obj_action()
    def read(self, source):
        source.display_line(self.text, "tell_to")
