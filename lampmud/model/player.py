from lampost.di.resource import Injected, module_inject
from lampost.gameops.target import TargetKeys
from lampost.meta.auto import AutoField
from lampost.db.dbofield import DBOField
from lampost.user.model import Player
from lampost.util.display import display_dto

from lampmud.model.entity import Entity
from lampmud.model.item import ItemDBO

log = Injected('log')
ev = Injected('dispatcher')
module_inject(__name__)


class MudPlayer(Player, Entity, ItemDBO):
    dbo_key_type = 'player'

    group = AutoField()

    is_player = True
    can_die = True
    instance_id = DBOField(0)
    instance_room_id = DBOField()
    room_id = DBOField()
    home_room = DBOField()

    def _on_attach(self):
        self.target_keys = TargetKeys(self.dbo_id)

    def _on_enter_env(self, new_env, *_):
        if self.instance:
            self.instance_id = self.instance.instance_id
            self.instance_room_id = new_env.dbo_id
        elif new_env.dbo_id:
            self.room_id = new_env.dbo_id
            self.instance_id = None
        self.output('current_env', display_dto(new_env, self))

    @property
    def location(self):
        try:
            return getattr(self.env, "title")
        except AttributeError:
            return "Unknown"

    def long_desc(self, *_):
        return self.short_desc()

    def short_desc(self, *_):
        return '{}, {}'.format(self.name, self.title or 'An Immortal' if self.imm_level else 'A player')

    def die(self):
        pass

