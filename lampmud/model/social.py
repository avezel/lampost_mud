from lampost.db.dbo import KeyDBO, OwnerDBO
from lampost.db.dbofield import DBOField

from lampmud.action.social import add_social_action, remove_social_action
from lampmud.engine.broadcast import BroadcastMap


class Social(OwnerDBO):
    dbo_key_type = 'social'
    dbo_set_key = 'socials'

    b_map = DBOField({})

    msg_class = 'social'

    def _on_hydrated(self):
        self.broadcast_map = BroadcastMap(**self.b_map)

    def _on_db_created(self):
        add_social_action(self)

    def _on_db_deleted(self):
        remove_social_action(self)

    def __call__(self, source, target, **_):
        source.broadcast(target=target, broadcast_map=self.broadcast_map)
