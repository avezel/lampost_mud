from importlib import import_module


def app_setup(args, flavor_package):
    import lampmud.model
    lampmud.model.init_model()

    first_player = None
    if flavor_package:
        flavor_module = import_module(flavor_package)
        flavor_setup = getattr(flavor_module, 'flavor_setup')
        if flavor_setup:
            first_player = flavor_setup(args)
    return first_player


def app_start(args, flavor_package):
    from lampost.di import resource
    from lampmud.editor.server import add_editor_routes

    from lampmud.action import add_actions
    from lampmud.engine import instance, attach_listeners
    from lampmud.model import init_model

    resource.register('instance_manager', instance)

    init_model()
    add_actions()
    add_editor_routes()
    attach_listeners()

    if flavor_package:
        flavor_module = import_module(flavor_package)
        flavor_start = getattr(flavor_module, 'flavor_start')
        if flavor_start:
            flavor_start(args)
