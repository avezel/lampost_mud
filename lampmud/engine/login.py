from lampost.db.exceptions import ObjectExistsError
from lampost.di.app import on_app_start
from lampost.service.channel import Channel
from lampost.di.config import config_value
from lampost.di.resource import Injected, module_inject
from lampost.util.display import display_dto

from lampmud.action.store import imm_actions

log = Injected('log')
db = Injected('datastore')
ev = Injected('dispatcher')
perm = Injected('perm')
um = Injected('user_manager')
instance_manager = Injected('instance_manager')
message_service = Injected('message_service')
friend_service = Injected('friend_service')
module_inject(__name__)


@on_app_start(priority=2000)
def _start():
    global shout_channel, imm_channel
    shout_channel = Channel('shout', general=True)
    imm_channel = Channel('imm')
    ev.register('name_test', _name_test)
    ev.register('player_create', _player_create)
    ev.register('player_attach', _player_attach, priority=-100)
    ev.register('player_connect', _player_connect)
    ev.register('missing_env', _start_env)
    ev.register('imm_attach', _imm_attach, priority=-50)
    ev.register('check_logout', _check_logout)


def _player_create(player, user):
    if len(user.player_ids) == 1 and not player.imm_level:
        player.imm_level = perm.perm_level('builder')
        perm.update_immortal_list(player)
        ev.dispatch('imm_update', player, 0)
        message_service.add_message('system', "Welcome!  Your first player has been given immortal powers.  Check out the 'Editor' window on the top menu.", player.dbo_id)
    player.room_id = config_value('default_start_room')


def _name_test(account_name, user):
    if  db.object_exists('area', account_name):
        raise ObjectExistsError


def _player_attach(player):
    shout_channel.add_sub(player)
    ev.dispatch('imm_attach', player, 0)
    player.change_env(_start_env(player))


def _player_connect(player, *_):
    player.status_change()
    player.output('current_env', display_dto(player.env, player))


def _imm_attach(player, old_level):
    player.can_die = player.imm_level == 0
    player.immortal = not player.can_die
    if player.immortal and not old_level:
        imm_channel.add_sub(player)
    elif not player.immortal and old_level:
        imm_channel.remove_sub(player)
    for cmd in imm_actions:
        if player.imm_level >= perm.perm_level(cmd.imm_level):
            player.enhance_soul(cmd)
        else:
            player.diminish_soul(cmd)


def _start_env(player):
    instance = instance_manager.get(player.instance_id)
    instance_room = db.load_object(player.instance_room_id, 'room', silent=True)
    player_room = db.load_object(player.room_id, 'room', silent=True)

    if instance and instance_room:
        # Player is returning to an instance still in memory
        return instance.get_room(instance_room)

    if instance_room and not player_room:
        # Player has no 'non-instanced' room, so presumably was created in a new instanced tutorial/racial area
        instance = instance_manager.next_instance()
        return instance.get_room(instance_room)

    # If we get here whatever instance data was associated with the player is no longer valid
    del player.instance_id
    del player.instance_room_id

    if player_room:
        return player_room

    return db.load_object(config_value('default_start_room'), 'room')


def _check_logout(player):
    player.check_logout()
