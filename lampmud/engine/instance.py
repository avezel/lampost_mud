from lampost.di.app import on_app_start
from lampost.di.config import ConfigVal
from lampost.di.resource import Injected, module_inject


log = Injected('log')
db = Injected('datastore')
ev = Injected('dispatcher')
module_inject(__name__)

preserve_hours = ConfigVal('instance_preserve_hours')
instance_map = {}


@on_app_start
def _start():
    ev.register('maintenance', remove_old)


def next_instance():
    instance_id = db.db_counter('instance_id')
    area_instance = AreaInstance(instance_id)
    instance_map[instance_id] = area_instance
    return area_instance


def remove_old():
    stale_pulse = ev.future_pulse(preserve_hours.value * 60 * 60)
    for instance_id, instance in instance_map.copy().items():
        if instance.pulse_stamp < stale_pulse and not [entity for entity in instance.entities
                                                       if hasattr(entity, 'is_player') and entity.session]:
            delete(instance_id)


def get(instance_id):
    return instance_map.get(instance_id)


def delete(instance_id):
    del instance_map[instance_id]


class AreaInstance():
    def __init__(self, instance_id):
        self.instance_id = instance_id
        self.entities = set()
        self.rooms = {}
        self.pulse_stamp = ev.current_pulse

    def add_entity(self, entity):
        self.entities.add(entity)
        entity.instance = self
        self.pulse_stamp = ev.current_pulse

    def remove_entity(self, entity):
        if entity in self.entities:
            del entity.instance
            self.entities.remove(entity)
            if not self.entities:
                self.clear_rooms()
                if entity.group:
                    entity.group.instance = None
                delete(self.instance_id)

    def clear_rooms(self):
        for room in self.rooms.values():
            room.detach()

    def get_room(self, room):
        if not room:
            log.error("Null room passed to area instance")
            return
        try:
            my_room = self.rooms[room.dbo_id]
        except KeyError:
            my_room = room.clone()
            my_room.instance = self
            self.rooms[room.dbo_id] = my_room
        return my_room
