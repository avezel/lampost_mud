import html

from lampost.server.link import add_link_route


def add_routes():
    add_link_route('action', _action)


def _action(player, action, **_):
    player.parse(html.escape(action.strip(), False))
