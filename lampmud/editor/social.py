from lampost.db.exceptions import DataError
from lampost.di.resource import Injected, module_inject
from lampost.editor.editor import Editor

from lampmud.engine.broadcast import BroadcastMap, Broadcast, broadcast_types

mud_actions = Injected('mud_actions')
module_inject(__name__)


class SocialsEditor(Editor):
    def __init__(self):
        super().__init__('social')

    @staticmethod
    def preview(source, target, b_map, self_source, **_):
        broadcast = Broadcast(BroadcastMap(**b_map), source, source if self_source else target)
        return {broadcast_type['id']: broadcast.substitute(broadcast_type['id']) for broadcast_type in broadcast_types}

    def _pre_create(self, obj_def, *_):
        if mud_actions.primary(obj_def['dbo_id']):
            raise DataError("Verb already in use")
