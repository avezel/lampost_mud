import sys
import lampost.bootstrap.appsetup

if __name__ != "__main__":
    print("Invalid usage")
    sys.exit(2)


lampost.bootstrap.appsetup.start_setup('lampmud', 'lampmud.lpmud')
