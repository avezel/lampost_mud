import sys
import lampost.bootstrap.appstart

if __name__ != "__main__":
    print("Invalid usage")
    sys.exit(2)


lampost.bootstrap.appstart.start_app('lampmud', 'lampmud.lpmud')
